/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto.dao;

import org.hibernate.Session;
import proyecto.entidades.Asesor;
import proyecto.util.HibernateUtil;


/**
 *
 * @author Luiggy
 */
public class AsesorDao extends Dao<Asesor> implements IAsesorDao {
    public AsesorDao() {
        super(Asesor.class);  
}
     @Override
    public int getNextId() {
        Session session = HibernateUtil.getSession();
        int id = (int) session.createQuery("select case when count(a.id_asesor) = 0 then 0 else max(a.id_asesor) end from Asesor a ").list().get(0);
        return id + 1;
    }
}

