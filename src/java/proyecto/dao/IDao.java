/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto.dao;

import java.util.List;

/**
 *
 * @author Luiggy
 */
public interface IDao<T> {
    void persist(T entity);

    void persistRange(List<T> entities);

    void merge(T entity);

    void delete(T entity);

    void deleteRange(List<T> entities);

    T find(int entityID);

    List<T> findAll();
    
}
