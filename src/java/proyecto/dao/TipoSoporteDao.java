/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto.dao;

import org.hibernate.Session;
import proyecto.entidades.TipoSoporte;
import proyecto.util.HibernateUtil;

/**
 *
 * @author Luiggy
 */
public class TipoSoporteDao extends Dao<TipoSoporte> implements ITipoSoporteDao {
    public TipoSoporteDao() {
        super(TipoSoporte.class);  
    
}
     @Override
    public int getNextId() {
        Session session = HibernateUtil.getSession();
        int id = (int) session.createQuery("select case when count(t.id_tiposoporte) = 0 then 0 else max(t.id_tiposoporte) end from tipo_soporte t ").list().get(0);
        return id + 1;
    }
}
