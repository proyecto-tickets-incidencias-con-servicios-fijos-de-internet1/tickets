/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto.dao;

import proyecto.entidades.Escenarios;

/**
 *
 * @author Luiggy
 */
public interface IEscenarioDao extends IDao<Escenarios> {
    int getNextId();
}
