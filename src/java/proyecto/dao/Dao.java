/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto.dao;

import java.util.List;
import org.hibernate.Session;
import proyecto.util.HibernateUtil;

/**
 *
 * @author Luiggy
 */
public class Dao<T> implements IDao<T> {

    private final Class<T> clase;

    public Dao(Class<T> clase) {
        this.clase = clase;
    }

    @Override
    public void persist(T entity) {
        try {
            Session session = HibernateUtil.getSession();
            session.beginTransaction();
            session.save(entity);
            session.getTransaction().commit();
            session.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public void merge(T entity) {
        try {
            Session session = HibernateUtil.getSession();
            session.beginTransaction();
            session.update(entity);
            session.getTransaction().commit();
            session.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void delete(T entity) {
        try {
            Session session = HibernateUtil.getSession();
            session.beginTransaction();
            session.delete(entity);
            session.getTransaction().commit();
            session.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public T find(int entityID) {
        Session session = HibernateUtil.getSession();
        T entity = (T) session.get(clase, entityID);
        session.close();
        return entity;
    }

    @Override
    public List<T> findAll() {
        Session session = HibernateUtil.getSession();
        List<T> entities = session.createQuery("from " + clase.getName()).list();
        session.close();
        return entities;
    }

    @Override
    public void persistRange(List<T> entities) {
        try {
            Session session = HibernateUtil.getSession();
            session.beginTransaction();
            for (T entity : entities) {
                session.save(entity);
            }
            session.getTransaction().commit();
            session.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public void deleteRange(List<T> entities) {
        try {
            Session session = HibernateUtil.getSession();
            session.beginTransaction();
            for (T entity : entities) {
                session.delete(entity);
            }
            session.getTransaction().commit();
            session.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

}

