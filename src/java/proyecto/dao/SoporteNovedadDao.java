/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto.dao;

import org.hibernate.Session;
import proyecto.entidades.SoporteNovedad;
import proyecto.util.HibernateUtil;

/**
 *
 * @author Luiggy
 */
public class SoporteNovedadDao extends Dao<SoporteNovedad> implements ISoporteNovedadDao{
    public SoporteNovedadDao() {
        super(SoporteNovedad.class);  
    
}
     @Override
    public int getNextId() {
        Session session = HibernateUtil.getSession();
        int id = (int) session.createQuery("select case when count(n.id_soportenovedad) = 0 then 0 else max(n.id_soportenovedad) end from soporte_novedad n ").list().get(0);
        return id + 1;
    }
}
