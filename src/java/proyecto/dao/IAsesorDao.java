/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto.dao;

import proyecto.entidades.Asesor;



/**
 *
 * @author Luiggy
 */
public interface IAsesorDao extends IDao <Asesor> {
    int getNextId();
}
