/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto.dao;

import org.hibernate.Session;
import proyecto.entidades.Cliente;
import proyecto.util.HibernateUtil;

/**
 *
 * @author Luiggy
 */
public class ClienteDao extends Dao<Cliente> implements IClienteDao {
    
    public ClienteDao() {
        super(Cliente.class);  
}
    @Override
    public int getNextId() {
        Session session = HibernateUtil.getSession();
        int id = (int) session.createQuery("select case when count(c.id_cliente) = 0 then 0 else max(c.id_cliente) end from Cliente c ").list().get(0);
        return id + 1;
    }
}

    
