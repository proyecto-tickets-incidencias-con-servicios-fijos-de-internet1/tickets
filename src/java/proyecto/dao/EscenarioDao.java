/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto.dao;

import org.hibernate.Session;
import proyecto.entidades.Escenarios;
import proyecto.util.HibernateUtil;

/**
 *
 * @author Luiggy
 */
public class EscenarioDao extends Dao<Escenarios> implements IEscenarioDao {
     public EscenarioDao() {
        super(Escenarios.class);  
}
      @Override
    public int getNextId() {
        Session session = HibernateUtil.getSession();
        int id = (int) session.createQuery("select case when count(s.id_asesor) = 0 then 0 else max(s.id_asesor) end from Escenarios s ").list().get(0);
        return id + 1;
    }
}
