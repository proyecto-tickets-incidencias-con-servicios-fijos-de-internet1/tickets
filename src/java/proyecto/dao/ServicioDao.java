/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto.dao;

import org.hibernate.Session;
import proyecto.entidades.Servicios;
import proyecto.util.HibernateUtil;

/**
 *
 * @author Luiggy
 */
public class ServicioDao extends Dao<Servicios> implements IServicioDao {

    public ServicioDao() {
        super(Servicios.class);
    }

    @Override
    public int getNextId() {
        Session session = HibernateUtil.getSession();
        int id = (int) session.createQuery("select case when count(s.id_servicios) = 0 then 0 else max(s.id_asesor) end from Servicios s ").list().get(0);
        return id + 1;
    }
}
