/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto.repositorio;

import proyecto.dao.AsesorDao;
import proyecto.dao.ClienteDao;
import proyecto.dao.EscenarioDao;
import proyecto.dao.IAsesorDao;
import proyecto.dao.IClienteDao;
import proyecto.dao.IEscenarioDao;
import proyecto.dao.IServicioDao;
import proyecto.dao.ISoporteNovedadDao;
import proyecto.dao.ITipoSoporteDao;
import proyecto.dao.ServicioDao;
import proyecto.dao.SoporteNovedadDao;
import proyecto.dao.TipoSoporteDao;

/**
 *
 * @author Luiggy
 */
public class Repository implements IRepository {

    private final IClienteDao clientes;
    private final IAsesorDao asesores;
    private final IServicioDao servicios;
    private final ISoporteNovedadDao novedadesdesoporte;
    private final ITipoSoporteDao tiposdesoportes;
    private final IEscenarioDao escenarios;

    public Repository() {
        this.clientes = new ClienteDao();
        this.asesores = new AsesorDao();
        this.servicios = new ServicioDao();
        this.novedadesdesoporte = new SoporteNovedadDao();
        this.tiposdesoportes = new TipoSoporteDao();
        this.escenarios = new EscenarioDao();
    }

    @Override
    public IClienteDao clientes() {
        return this.clientes;
    }

    @Override
    public IAsesorDao asesores() {
        return this.asesores;
    }

    @Override
    public ITipoSoporteDao tiposdesoportes() {
        return this.tiposdesoportes;
    }

    //Verificar 
    @Override
    public ISoporteNovedadDao novedadesdesoporte() {
        return this.novedadesdesoporte;
    }

    @Override
    public IServicioDao servicios() {
        return this.servicios;
    }

    @Override
    public IEscenarioDao escenarios() {
        return this.escenarios;
    }
}
