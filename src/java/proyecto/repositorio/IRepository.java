/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto.repositorio;

import proyecto.dao.IAsesorDao;
import proyecto.dao.IClienteDao;
import proyecto.dao.IEscenarioDao;
import proyecto.dao.IServicioDao;
import proyecto.dao.ISoporteNovedadDao;
import proyecto.dao.ITipoSoporteDao;

/**
 *
 * @author Luiggy
 */
public interface IRepository {
    
    IClienteDao clientes ();
    
    IAsesorDao asesores();
    
    ITipoSoporteDao tiposdesoportes();
    
    ISoporteNovedadDao novedadesdesoporte();
    
    IServicioDao servicios();
    
    IEscenarioDao escenarios();
    
    
}
