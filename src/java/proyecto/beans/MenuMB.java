/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto.beans;

import javax.faces.bean.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author Luiggy
 */
@Named
@ViewScoped
public class MenuMB implements java.io.Serializable {
    private String url="";

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    
}
